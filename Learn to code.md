# Learn to code
Some resources to help you learn to code on the web.

### JAVASCRIPT
[Khan Academy's unit on JS](https://www.khanacademy.org/computing/computer-programming/programming) is where I'd suggest you start out.

[Learnjavascript.online](https://learnjavascript.online/) is another great way to learn Javascript.

[W3Schools](https://www.w3schools.com/js/default.asp) goes for a test-things-out system, instead of a solve-the-puzzle system.

### HTML/CSS
[Khan Academy's unit on HTML and CSS](https://www.khanacademy.org/computing/computer-programming/html-css) is where I'd say the average person should start out.

[Learn-html.org](https://www.learn-html.org/) is a tutorial site on the simpler side when it comes to tutorials. Read the page, do the exercise (if it has one, some of them don't yet sadly), repeat.

W3Schools's [HTML](https://www.w3schools.com/html/default.asp) and [CSS](https://www.w3schools.com/css/default.asp) tutorials go for a test-things-out system, instead of a solve-the-puzzle system.